<?php

defined('_JEXEC') or die('Restricted access');

require_once JPATH_COMPONENT_SITE . '/domain/HelloWorldService.php';
require_once JPATH_COMPONENT_SITE . '/domain/iHelloWorldRepository.php';
require_once JPATH_COMPONENT_SITE . '/repository/HelloWorldRepository.php';

class HelloWorldModelHelloWorld extends JModelItem
{
    private $helloWorldService;

    protected $messages;

    function __construct()
    {
      $helloWorldRepository = new HelloWorldRepository();
      $this->helloWorldService = new HelloWorldService($helloWorldRepository);

      parent::__construct();
    }

    public function getTable ($type = 'HelloWorld', $prefix = 'HelloWorldTable', $config = array())
    {
        return JTable::getInstance($type, $prefix, $config);
    }

    public function getMsg()
    {
      if (!is_array($this->messages))
      {
          $this->messages = array();
      }

      if (!isset($this->messages[$id]))
      {
          $id = $this->getHelloWorldPublishedId();

          if (isset($id)) {
            $table = $this->getTable();
            $table->load($id);
            $this->messages[$id] = $table->greeting;
          }
          else {
            $id = 0;
            $this->messages[$id] = '';
          }
      }

      return $this->messages[$id];
    }

    private function getHelloWorldPublishedId()
    {
      return $this->helloWorldService->getPublishedId();
    }
}
