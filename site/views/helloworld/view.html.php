<?php
defined('_JEXEC') or die('Restricted access');

class HelloWorldViewHelloWorld extends JViewLegacy
{
    function display($tpl = null)
    {
        $this->msg = $this->get('Msg');

        parent::display($tpl);
    }
}
