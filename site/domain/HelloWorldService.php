<?php

class HelloWorldService
{
  private $helloWorldRepository;

  function __construct($helloWorldRepository)
  {
    $this->helloWorldRepository = $helloWorldRepository;
  }

  public function getPublishedId()
  {
    return $this->helloWorldRepository->getHelloWorldPublishedId();
  }
}
