<?php

defined('_JEXEC') or die('Restricted access');

class HelloWorldModelHelloWorlds extends JModelList
{

  function getListQuery()
  {
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);

    $query->select('*')
    ->from($db->quoteName('#__helloworld2'));

    return $query;
  }
}
