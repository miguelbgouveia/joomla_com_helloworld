<?php
defined('_JEXEC') or die('Restricted access');

class HelloWorldViewHelloWorld extends JViewLegacy
{
  protected $form = null;

  public function display($tpl = null)
  {
    $this->form = $this->get('Form');
    $this->item = $this->get('Item');

    if (count($errors = $this->get('Errors'))) {
      JError::raiseError(500, implode('<br />', $errors));

      return false;
    }

    $this->addToolBar();

    parent::display($tpl);
  }

  protected function addToolBar()
  {
    $input = JFactory::getApplication()->input;

    $input->set('hidemainmenu', true);

    $isNew = ($this->item->id == 0);

    if ($isNew) {
      $title = JText::_('COM_HELLOWORLD_MANAGER_HELLOWORLD_NEW');
    }
    else {
      $title = JText::_('COM_HELLOWORLD_MANAGER_HELLOWORLD_EDIT');
    }

    JToolBarHelper::title($title, 'helloworld');
    JToolBarHelper::save('helloworld.save');
    JToolBarHelper::cancel(
      'helloworld.cancel',
      $isNew ? 'JTOOLBAR_CANCEL' : 'JTOOLBAR_CLOSE'
    );
    
  }
}
