<?php

require_once JPATH_COMPONENT_SITE . '/domain/iHelloWorldRepository.php';

class HelloWorldRepository implements iHelloWorldRepository
{

  public function getHelloWorldPublishedId()
  {
    $db = JFactory::getDbo();

    $query = $db->getQuery(true);

    $query->select($db->quoteName('id'))
          ->from($db->quoteName('#__helloworld2'))
          ->where($db->quoteName('published') . ' = 1')
          ->setLimit('1');

    $db->setQuery($query);

    $id = $db->loadResult();

    return $id;
  }

}
